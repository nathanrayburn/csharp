﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Maths
{
    [TestClass]
    public class TestCalculator
    {
        #region private attributes
        private int testOp1;
        private int testOp2;
        private int testExpectedResult;
        private int testActualResult;
        #endregion private attributes
        [TestMethod]
        public void Test_Add_Success()
        {
            //given
            Calculator testCalculator = new Calculator();
            testOp1 = 2;
            testOp2 = 4;
            testExpectedResult = 6;
            //when
            testActualResult = testCalculator.add(testOp1, testOp2);
            //then
            Assert.AreEqual(testExpectedResult, testActualResult);
        }

        [TestMethod]
        public void Test_Sub_Success()
        {
            //given
            Calculator testCalculator = new Calculator();
            testOp1 = 8;
            testOp2 = 4;
            testExpectedResult = 4;
            //when
            testActualResult = testCalculator.sub(testOp1, testOp2);
            //then
            Assert.AreEqual(testExpectedResult, testActualResult);
        }

        [TestMethod]
        public void Test_Mul_Success()
        {
            //given
            Calculator testCalculator = new Calculator();
            testOp1 = 2;
            testOp2 = 4;
            testExpectedResult = 8;
            //when
            testActualResult = testCalculator.mul(testOp1, testOp2);
            //then
            Assert.AreEqual(testExpectedResult, testActualResult);
        }

        [TestMethod]
        public void Test_Div_Success()
        {
            //given
            Calculator testCalculator = new Calculator();
            testOp1 = 8;
            testOp2 = 4;
            testExpectedResult = 2;
            //when
            testActualResult = testCalculator.div(testOp1, testOp2);
            //then
            Assert.AreEqual(testExpectedResult, testActualResult);
        }
        [TestMethod]

        public void createCalc()
        {
            //Given

            //when
           // Calculations testCalc = new Calculations(testOp1,testOpe,testOp2,res);

            //then



        }

        [TestMethod]

        public void history()
        {
            //given
            int FirstOperand = 1;

            string Ope = "+";

            int SecondOperand = 2;

            int resultat = 3;

            Calculation testList = new Calculation(FirstOperand,Ope,SecondOperand,resultat);
            int exceptedAmountOfFirstname = 1;
            int actualAmountOfObjects;
            //when
            
            actualAmountOfObjects = testList.actualAmountOfObjects();
            //then
            Assert.AreEqual(actualAmountOfObjects, exceptedAmountOfFirstname);



        }
    }
}
