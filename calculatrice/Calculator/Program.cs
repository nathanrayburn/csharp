﻿/*  Title  :    Calculator
 *  Author :    nicolas.glassey@cpnv.ch
 *  Version:    06.09.2018 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Maths
{
    class Program
    {
        #region private attributes
        static private int userInputOperand01;
        static private int userInputOperand02;
        static private int displayResult;
        #endregion private attributes

        /// <summary>
        /// This function is designed to be application's entry point
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //ask user to input 2 operands
            Console.Write("First operand  : ");
            userInputOperand01 = int.Parse(Console.ReadLine());

            Console.Write("Operator : ");
            string userInputOperator = Console.ReadLine();

            Console.Write("Second operand : ");
            userInputOperand02 = int.Parse(Console.ReadLine());
            
            string displayResult = calculateUserInput(userInputOperator, userInputOperand01, userInputOperand02);
         
            //display result
            Console.Write(displayResult);
            Console.ReadLine();
        }



        public static string calculateUserInput(string op, int op1, int op2)
        {
            int result;
            Calculator newCalculator = new Calculator();

            switch (op)
            {
                case "+":
                    result = newCalculator.add(op1, op2);
                    Console.WriteLine("Result of " + op1 + " + " + op2 + " = " + result);
                    return op1 + op2 + op + result;
                case "-":
                    result = newCalculator.sub(op1, op2);
                    Console.WriteLine("Result of " + op1 + " - " + op2 + " = " + result);
                    return op1 + op2 + op + result;
                case "*":
                    result = newCalculator.mul(op1, op2);
                    Console.WriteLine("Result of " + op1 + " * " + op2 + " = " + result);
                    return op1 + op2 + op + result;
                case "/":
                    result = newCalculator.div(op1, op2);
                     Console.WriteLine("Result of " + op1 + " / " + op2 + " = " + result);
                    
                    return op1 + op2 + op + result;
                default:
                    return "There is an error somewhere, please check your numbers";
            }
            
         
        }




    }
}
        
