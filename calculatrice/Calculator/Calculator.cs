﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maths
{
    public class Calculator
    {
        #region public methods
        public int add(int op1, int op2)
        {
            return op1 + op2;
        }
        public int sub(int op1, int op2)
        {
            return op1 - op2;
        }
        public int mul(int op1, int op2)
        {
            return op1 * op2;
        }
        public int div(int op1, int op2)
        {
            
            return op1 / op2;
        }
        #endregion public methods
    }

    public class Calculation
    {
        #region private attributes
        private int FirstOperand;
        private string Ope;
        private int SecondOperand;
        private int resultat;
        private List<Calculation> history;
        #endregion private attributes



        #region public methods
        
        #region constructor
        //Object of a calculation

        public Calculation(int FirstOperand, string Ope, int SecondOperand, int resultat)
        {

            //properties of the object 
            this.FirstOperand = FirstOperand;
            this.Ope = Ope;
            this.SecondOperand = SecondOperand;
            this.resultat = resultat;

          

       
        }

        //add an object into a list 
        public void addHistory(Calculation calc) => history.Add(calc);

        public int actualAmountOfObjects()
        {

            return 1;
        }


        #endregion construtor

        #endregion public methods

    }

}
